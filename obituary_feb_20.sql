-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 20, 2019 at 08:25 AM
-- Server version: 5.7.21
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `obituary`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_preferences`
--

CREATE TABLE `admin_preferences` (
  `id` tinyint(1) NOT NULL,
  `user_panel` tinyint(1) NOT NULL DEFAULT '0',
  `sidebar_form` tinyint(1) NOT NULL DEFAULT '0',
  `messages_menu` tinyint(1) NOT NULL DEFAULT '0',
  `notifications_menu` tinyint(1) NOT NULL DEFAULT '0',
  `tasks_menu` tinyint(1) NOT NULL DEFAULT '0',
  `user_menu` tinyint(1) NOT NULL DEFAULT '1',
  `ctrl_sidebar` tinyint(1) NOT NULL DEFAULT '0',
  `transition_page` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_preferences`
--

INSERT INTO `admin_preferences` (`id`, `user_panel`, `sidebar_form`, `messages_menu`, `notifications_menu`, `tasks_menu`, `user_menu`, `ctrl_sidebar`, `transition_page`) VALUES
(1, 0, 0, 0, 0, 0, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) UNSIGNED NOT NULL,
  `contact_holder_id` int(11) NOT NULL DEFAULT '0',
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `dob` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `contact_holder_id`, `firstName`, `lastName`, `email`, `gender`, `address`, `dob`) VALUES
(1, 1, 'Mohibul', 'Hasan', NULL, NULL, NULL, NULL),
(2, 1, 'Anwar', 'Biddut', '', NULL, NULL, NULL),
(3, 1, 'Elias Chowdhury', 'Eli', NULL, NULL, NULL, NULL),
(4, 1, 'Anik', 'Khan', NULL, NULL, NULL, NULL),
(5, 1, 'Naimah', 'Tanzim', NULL, NULL, NULL, NULL),
(6, 1, 'Yasir', 'Taher', NULL, NULL, NULL, NULL),
(7, 1, 'Orchid', 'Razzaq', NULL, NULL, NULL, NULL),
(8, 1, 'pijush', 'dutta', NULL, NULL, NULL, NULL),
(9, 1, 'H', '2', '', NULL, NULL, NULL),
(10, 1, 'H', '1', '', NULL, NULL, NULL),
(11, 1, 'Majo', 'Apa', '', NULL, NULL, NULL),
(12, 1, 'Ch', 'mama', '', NULL, NULL, NULL),
(13, 1, 'Shohel', '2', '', NULL, NULL, NULL),
(14, 1, 'Asif', 'citie', '', NULL, NULL, NULL),
(15, 1, 'Tito', 'chakrabarty', '', NULL, NULL, NULL),
(16, 1, 'Raz', 'tnt', '', NULL, NULL, NULL),
(17, 1, 'Mamun', 'Debt', '', NULL, NULL, NULL),
(18, 1, 'Shuvo', 'debt', '', NULL, NULL, NULL),
(19, 1, 'Javedul', 'Ferdous', '', NULL, NULL, NULL),
(20, 1, 'Khala', 'mony', '', NULL, NULL, NULL),
(21, 1, 'Ammu', 'air', '', NULL, NULL, NULL),
(22, 1, 'Hesham', 'vai', '', NULL, NULL, NULL),
(23, 1, 'Javedul', 'Ferdous', '', NULL, NULL, NULL),
(24, 1, 'Asif', 'gram3', '', NULL, NULL, NULL),
(25, 1, 'Raz', 'gram', '', NULL, NULL, NULL),
(26, 1, 'Fupi', 'B', '', NULL, NULL, NULL),
(27, 1, 'Abbu', 'blink', '', NULL, NULL, NULL),
(28, 1, 'Mohibul', 'Hasan', '', NULL, NULL, NULL),
(29, 1, 'Ray', 'Cs', '', NULL, NULL, NULL),
(30, 1, 'Abbu', 'Gram', '', NULL, NULL, NULL),
(31, 1, 'Mejo', 'Mama', '', NULL, NULL, NULL),
(32, 1, 'Shapna', 'Apu', '', NULL, NULL, NULL),
(33, 1, 'Asif', 'Air', '', NULL, NULL, NULL),
(34, 1, 'G', 'N', '', NULL, NULL, NULL),
(35, 1, 'Choto', 'Fupi', '', NULL, NULL, NULL),
(36, 1, 'Raz', 'New', '', NULL, NULL, NULL),
(37, 1, 'imtiz', 'sir', '', NULL, NULL, NULL),
(38, 1, 'Armin', 'Vai', '', NULL, NULL, NULL),
(39, 1, 'B', 'Khala', '', NULL, NULL, NULL),
(40, 1, 'Abbu', 'Riyad', '', NULL, NULL, NULL),
(41, 1, 'Ray', 'Tel', '', NULL, NULL, NULL),
(42, 1, 'Mohammad Anwarul', 'Islam', '', NULL, NULL, NULL),
(43, 1, 'ray', 'robi', '', NULL, NULL, NULL),
(44, 1, 'global', 'net', '', NULL, NULL, NULL),
(45, 1, 'Ayan''s', 'Mom', '', NULL, NULL, NULL),
(46, 1, 'Ayan''s', 'Dad', '', NULL, NULL, NULL),
(47, 1, 'Arif', 'R', '', NULL, NULL, NULL),
(48, 1, 'B', 'Mama', '', NULL, NULL, NULL),
(49, 1, 'Ammu', 'Gram', '', NULL, NULL, NULL),
(50, 1, 'Bdjobs', 'Sal', '', NULL, NULL, NULL),
(51, 1, 'Arif', 'G', '', NULL, NULL, NULL),
(52, 1, 'Ayaan', 'Bl', '', NULL, NULL, NULL),
(53, 1, 'Canchal', 'B', '', NULL, NULL, NULL),
(54, 1, 'Tawsif', 'B', '', NULL, NULL, NULL),
(55, 1, 'Tithi', 'Apu', '', NULL, NULL, NULL),
(56, 1, 'Office', 'Ka', '', NULL, NULL, NULL),
(57, 1, 'Atik', 'Off', '', NULL, NULL, NULL),
(58, 1, 'Bdjobs', 'Sal', '', NULL, NULL, NULL),
(59, 1, 'Acc', 'Main', '', NULL, NULL, NULL),
(60, 1, 'Office', 'Sir', '', NULL, NULL, NULL),
(61, 1, 'Raz', 'gr', '', NULL, NULL, NULL),
(62, 1, 'ayan', 'bl2', '', NULL, NULL, NULL),
(63, 1, 'kamrul', 'off', '', NULL, NULL, NULL),
(64, 1, 'pij', 'off', '', NULL, NULL, NULL),
(65, 1, 'sifat', 'off', '', NULL, NULL, NULL),
(66, 1, 'irfan', 'off', '', NULL, NULL, NULL),
(67, 1, 'cycle', 'surg', '', NULL, NULL, NULL),
(68, 1, 'Mohibul', 'Hasan', 'mhpeash.ewu@gmail.com', NULL, NULL, NULL),
(69, 1, 'Elias Chowdhury', 'Eli', 'eliasmail09@gmail.com', NULL, NULL, NULL),
(70, 1, 'Anik', 'Khan', 'phoenex.khan@gmail.com', NULL, NULL, NULL),
(71, 1, 'Naimah', 'Tanzim', 'naimahtanzim@yahoo.com', NULL, NULL, NULL),
(72, 1, 'Yasir', 'Taher', 'yasir.yt@gmail.com', NULL, NULL, NULL),
(73, 1, 'Orchid', 'Razzaq', 'orchid.razzaq@gmail.com', NULL, NULL, NULL),
(74, 1, 'pijush', 'dutta', 'pijush.dutt@gmail.com', NULL, NULL, NULL),
(76, 4, 'jiayt', 'Muytr', 'jiayt@muytr.com', NULL, NULL, NULL),
(79, 1, 'Faijarul', 'Armin', '', NULL, NULL, NULL),
(80, 1, 'Pij', 'Grm', '', NULL, NULL, NULL),
(81, 1, 'M', 'Fupi', '', NULL, NULL, NULL),
(82, 1, 'Orchid', 'New', '', NULL, NULL, NULL),
(83, 1, 'Tanzim', 'Gram', '', NULL, NULL, NULL),
(84, 1, 'Global', 'Mech', '', NULL, NULL, NULL),
(85, 4, 'ranaga', 'trabab', 'ranaga@trabab.com', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) UNSIGNED NOT NULL,
  `contact_holder_id` int(11) NOT NULL DEFAULT '0',
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `contact_holder_id`, `firstName`, `lastName`, `email`, `created_on`, `updated_on`) VALUES
(1, 1, 'ashik', NULL, NULL, 0, 0),
(2, 1, 'Mohibul', 'Hasan', NULL, 0, 0),
(4, 1, 'Anwar', 'Biddut', '', 0, 0),
(8, 1, 'Naimah', 'Tanzim', NULL, 0, 0),
(9, 1, 'Yasir', 'Taher', NULL, 0, 0),
(10, 1, 'Orchid', 'Razzaq', NULL, 0, 0),
(11, 1, 'ashik', NULL, NULL, 0, 0),
(12, 1, 'Mohibul', 'Hasan', NULL, 0, 0),
(13, 1, '01894-85190', NULL, '', 0, 0),
(14, 1, 'Anwar', 'Biddut', '', 0, 0),
(15, 1, 'adnan', NULL, NULL, 0, 0),
(16, 1, 'Elias Chowdhury', 'Eli', NULL, 0, 0),
(17, 1, 'Anik', 'Khan', NULL, 0, 0),
(18, 1, 'Naimah', 'Tanzim', NULL, 0, 0),
(19, 1, 'Yasir', 'Taher', NULL, 0, 0),
(20, 1, 'Orchid', 'Razzaq', NULL, 0, 0),
(22, 1, 'Shfat', 'Sahajjo', 'shifat@sahajjo.com', 1532757369, 1532758599),
(23, 2, 'Fina', 'Monaco', 'fina@monaco.com', 1532869944, 0),
(24, 2, 'Talcom', 'Powder', 'talcom@powder.com', 1532869971, 0),
(25, 3, 'Hamid', 'Ansari', 'hamid@ansari.com', 1532871067, 0),
(26, 3, 'Rabi', 'Tagore', 'rabi@tagore.com', 1532871095, 0),
(27, 3, 'Nivea', 'Johnson', 'nivea@johnson.com', 1532871135, 0);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  `bgcolor` char(7) NOT NULL DEFAULT '#607D8B'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`, `bgcolor`) VALUES
(1, 'admin', 'Administrator', '#F44336'),
(2, 'members', 'General User', '#2196F3');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`id`, `ip_address`, `login`, `time`) VALUES
(1, '::1', 'admin@admin.lcom', 1539497812);

-- --------------------------------------------------------

--
-- Table structure for table `persons`
--

CREATE TABLE `persons` (
  `id` int(11) UNSIGNED NOT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `dob` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `persons`
--

INSERT INTO `persons` (`id`, `firstName`, `lastName`, `gender`, `address`, `dob`) VALUES
(1, 'Airi', 'Satou', 'female', 'Tokyo', '1964-03-04'),
(2, 'Garrett', 'Winters', 'male', 'Tokyo', '1988-09-02'),
(3, 'John', 'Doe', 'male', 'Kansas', '1972-11-02'),
(4, 'Tatyana', 'Fitzpatrick', 'male', 'London', '1989-01-01'),
(5, 'Quinn', 'Flynn', 'male', 'Edinburgh', '1977-03-24');

-- --------------------------------------------------------

--
-- Table structure for table `public_preferences`
--

CREATE TABLE `public_preferences` (
  `id` int(1) NOT NULL,
  `transition_page` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `public_preferences`
--

INSERT INTO `public_preferences` (`id`, `transition_page`) VALUES
(1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `search_result_log`
--

CREATE TABLE `search_result_log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `total` int(11) NOT NULL,
  `url` text,
  `log_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1539497824, 1, 'Admin', 'istrator', 'ADMIN', '0'),
(2, '::1', 'user1 demo', '$2y$08$EmDpHhJd.quO2MkyvZkHe.UnJ2YCz93B45pOz0FUKgcdEduCVig8S', NULL, 'user1@demo.com', NULL, NULL, NULL, NULL, 1532849077, 1532870717, 1, 'user1', 'demo', 'xx', '123123123123'),
(3, '::1', 'user2 demo', '$2y$08$EmDpHhJd.quO2MkyvZkHe.UnJ2YCz93B45pOz0FUKgcdEduCVig8S', NULL, 'user2@demo.com', NULL, NULL, NULL, NULL, 1532849130, 1532870991, 1, 'user2', 'demo', 'yy', '221221221221'),
(4, '::1', 'x demo', '$2y$08$cOHhkH9SKs8k6kFXZE8dROF/IQsNwNWPoi.YCYCSy.a4uzY/UcfBu', NULL, 'demoemailacc.xclient@gmail.com', NULL, NULL, NULL, NULL, 1539498498, 1539498573, 1, 'X', 'Demo', 'XD', '643132121');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(5, 3, 2),
(6, 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_contact_pagination`
--

CREATE TABLE `user_contact_pagination` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `page` int(11) NOT NULL,
  `fetched` int(11) NOT NULL,
  `completed` int(11) NOT NULL DEFAULT '0',
  `date` date DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_google_client_token`
--

CREATE TABLE `user_google_client_token` (
  `user_google_client_token_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `access_token` text,
  `refresh_token` text,
  `token_type` varchar(255) DEFAULT NULL,
  `expires_in` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `expires_at` int(11) NOT NULL,
  `token_json` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_google_client_token`
--

INSERT INTO `user_google_client_token` (`user_google_client_token_id`, `user_id`, `access_token`, `refresh_token`, `token_type`, `expires_in`, `created`, `expires_at`, `token_json`) VALUES
(1, 1, 'ya29.Gl02BtUhJA8Mseg9cg1AxG5PYJ3HtrW0-kDwWEttNCgxKQljINKqtIUSDAtlsXPVSPMUo74hnhCFrRn6_ohEdJMI8EmGW4Ff74Mx4wGkD1KvLqbyJiM72qhEcf12bL8', '1/ZnCxhepThh9BoRj-7fuN1s_Y4XVBWJ7fkJOZ4MGJdyZEbqgnLeKcetP03E3HAjna', 'Bearer', 3600, 1539501389, 1539504989, '{"access_token":"ya29.Gl02BtUhJA8Mseg9cg1AxG5PYJ3HtrW0-kDwWEttNCgxKQljINKqtIUSDAtlsXPVSPMUo74hnhCFrRn6_ohEdJMI8EmGW4Ff74Mx4wGkD1KvLqbyJiM72qhEcf12bL8","expires_in":3600,"scope":"https:\\/\\/www.googleapis.com\\/auth\\/contacts.readonly","token_type":"Bearer","created":1539501389,"refresh_token":"1\\/ZnCxhepThh9BoRj-7fuN1s_Y4XVBWJ7fkJOZ4MGJdyZEbqgnLeKcetP03E3HAjna"}'),
(3, 4, 'ya29.Gls2BotQwpN5tJYrRPa1ZO0Mjo3yoagmXGaQ2T2CfMUb0hUJ92pm6rJeXyWw3dZxjO7s8jeH4amJxxEH7Gco6yfrgeNAcm_yfA3iiCvxZNWQjQSoUXQ1OeUjH_LJ', '1/Or2lW4J98ydWpnQfXIucxQK7d2AcRmkTB-XqXGkCdHjfWGAAdNdlAtF2e_065ZCC', 'Bearer', 3600, 1539503550, 1539507150, '{"access_token":"ya29.Gls2BotQwpN5tJYrRPa1ZO0Mjo3yoagmXGaQ2T2CfMUb0hUJ92pm6rJeXyWw3dZxjO7s8jeH4amJxxEH7Gco6yfrgeNAcm_yfA3iiCvxZNWQjQSoUXQ1OeUjH_LJ","expires_in":3600,"refresh_token":"1\\/Or2lW4J98ydWpnQfXIucxQK7d2AcRmkTB-XqXGkCdHjfWGAAdNdlAtF2e_065ZCC","scope":"https:\\/\\/www.googleapis.com\\/auth\\/contacts.readonly","token_type":"Bearer","created":1539503550}');

-- --------------------------------------------------------

--
-- Table structure for table `user_processed_log`
--

CREATE TABLE `user_processed_log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `is_processed` int(11) NOT NULL DEFAULT '0',
  `processed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_preferences`
--
ALTER TABLE `admin_preferences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `persons`
--
ALTER TABLE `persons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `public_preferences`
--
ALTER TABLE `public_preferences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `search_result_log`
--
ALTER TABLE `search_result_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indexes for table `user_contact_pagination`
--
ALTER TABLE `user_contact_pagination`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_google_client_token`
--
ALTER TABLE `user_google_client_token`
  ADD PRIMARY KEY (`user_google_client_token_id`);

--
-- Indexes for table `user_processed_log`
--
ALTER TABLE `user_processed_log`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_preferences`
--
ALTER TABLE `admin_preferences`
  MODIFY `id` tinyint(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `persons`
--
ALTER TABLE `persons`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `public_preferences`
--
ALTER TABLE `public_preferences`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `search_result_log`
--
ALTER TABLE `search_result_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user_contact_pagination`
--
ALTER TABLE `user_contact_pagination`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_google_client_token`
--
ALTER TABLE `user_google_client_token`
  MODIFY `user_google_client_token_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_processed_log`
--
ALTER TABLE `user_processed_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
