<?php
defined('BASEPATH') OR exit('No direct script access allowed');


define('GOOGLE_CLIENT_LIB_PATH', APPPATH . '/libraries/google-api-php-client');
require_once GOOGLE_CLIENT_LIB_PATH . '/vendor/autoload.php';

class Contact extends Admin_Controller
{
//https://mbahcoding.com/tutorial/php/codeigniter/codeigniter-ajax-crud-using-bootstrap-modals-and-datatable.html
    public function __construct()
    {
        parent::__construct();
        $this->load->model('contact_model', 'contact');
        $this->load->helper('url');

        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }

        ini_set('MAX_EXECUTION_TIME', 270);

        /* Load :: Common */
        //$this->lang->load('admin/users');

        /* Title Page :: Common */
        $this->page_title->push(lang('menu_contact'));
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('menu_contact'), 'admin/users');
        $this->false_empty_or_null_arr = $this->false_empty_or_null_arr();
    }

    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }
        /* Breadcrumbs */
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        $user_id = $this->ion_auth->get_user_id();
        $this->data['access_token_row_array'] = $this->contact->getTokenRowArrayByUser($user_id);

        /* Load Template */
        $this->template->admin_render('admin/contact/contact_view', $this->data);

    }

    public function ajax_list()
    {
        $contact_holder_id = $this->ion_auth->get_user_id();

        $list = $this->contact->get_datatables($contact_holder_id);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $contact) {
            $no++;
            $row = array();
            $row[] = $contact->firstName;
            $row[] = $contact->lastName;
            $row[] = $contact->email;
            $row[] = $contact->gender;
            $row[] = $contact->address;
            $row[] = $contact->dob;

            //add html for action
            $contact->action = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_contact(' . "'" . $contact->id . "'" . ')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_contact(' . "'" . $contact->id . "'" . ')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
            $row[] = $contact->action;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->contact->count_all_for_a_contact_holder($contact_holder_id),
            "recordsFiltered" => $this->contact->count_filtered_for_a_contact_holder($contact_holder_id),
            "data" => $data,
        );
        //output to json format

        echo json_encode($output);
        die();
    }

    public function ajax_edit($id)
    {
        $data = $this->contact->get_by_id($id);
        $data->dob = ($data->dob == '0000-00-00') ? '' : $data->dob; // if 0000-00-00 set tu empty for datepicker compatibility
        echo json_encode($data);
        die();
    }

    public function ajax_add()
    {
        $this->_validate();
        $data = array(
            'firstName' => $this->input->post('firstName'),
            'lastName' => $this->input->post('lastName'),
            'gender' => $this->input->post('gender'),
            'email' => $this->input->post('email'),
            'address' => $this->input->post('address'),
            'dob' => $this->input->post('dob'),
            'contact_holder_id' => $this->ion_auth->get_user_id()
        );
        $insert = $this->contact->save($data);
        echo json_encode(array("status" => TRUE));
        die();
    }

    public function ajax_update()
    {
        $this->_validate();
        $data = array(
            'firstName' => $this->input->post('firstName'),
            'lastName' => $this->input->post('lastName'),
            'gender' => $this->input->post('gender'),
            'email' => $this->input->post('email'),
            'address' => $this->input->post('address'),
            'dob' => $this->input->post('dob'),
        );
        $this->contact->update(array('id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
        die();
    }

    public function ajax_delete($id)
    {
        $this->contact->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
        die();
    }


    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('firstName') == '') {
            $data['inputerror'][] = 'firstName';
            $data['error_string'][] = 'First name is required';
            $data['status'] = FALSE;
        }

        if ($this->input->post('lastName') == '') {
            $data['inputerror'][] = 'lastName';
            $data['error_string'][] = 'Last name is required';
            $data['status'] = FALSE;
        }

        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    private function false_empty_or_null_arr()
    {
        return [false, null, ""];
    }

    private function get_client_obj()
    {
        $client = new Google_Client();
        $client->setScopes(Google_Service_PeopleService::CONTACTS_READONLY);
        $client->setAuthConfig(GOOGLE_CLIENT_LIB_PATH . '/client_id.json');
        $client->setAccessType('offline');

        return $client;
    }

    public function authoriseGoogleContact()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }

        $client = $this->get_client_obj();

        $user_id = $this->ion_auth->get_user_id();

        $token_row_array = $this->contact->getTokenRowArrayByUser($user_id);


        if (!empty($token_row_array)) {

            $this->session->set_flashdata('success', 'success');
            $this->session->set_flashdata('access_token_already_exists', 'Access token already exist');
            redirect('admin/contact');
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();

            redirect($authUrl);
            exit;


        }

    }

    public function googleContactCallBack()
    {
        $successful = false;
        $client = $this->get_client_obj();
        $accessToken = $client->fetchAccessTokenWithAuthCode($_GET['code']);

        $client->authenticate($_GET['code']);
        $accessToken = $client->getAccessToken();

        if (!empty($accessToken)) {
            $accessToken_data = array();

            $accessToken_data['access_token'] = $accessToken['access_token'];
            $accessToken_data['refresh_token'] = $accessToken['refresh_token'];
            $accessToken_data['token_type'] = $accessToken['token_type'];
            $accessToken_data['expires_in'] = $accessToken['expires_in'];
            $accessToken_data['created'] = $accessToken['created'];


            $accessToken_data['user_id'] = $this->ion_auth->get_user_id();
            $accessToken_data['expires_at'] = $accessToken['created'] + $accessToken['expires_in'];
            $accessToken_data['token_json'] = json_encode($accessToken);

            $this->db->insert('user_google_client_token', $accessToken_data);

            $successful = true;
        }

        if ($successful) {
            $this->session->set_flashdata('success', 'success');
            $this->session->set_flashdata('contact_access_permission_successful', 'Successfully given the permission to read Google contacts');
        } else {
            $this->session->set_flashdata('error', 'error');
            $this->session->set_flashdata('contact_access_permission_unsuccessful', 'Could not get the permission to read Google contacts');
        }

        redirect('admin/contact');

    }


    public function importGoogleContact($user_id = null)
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }

        $client = $this->get_client_obj();

        $expired = false;

        if (!$user_id) {
            $user_id = $this->ion_auth->get_user_id();
        }

        $access_token_row_array = $this->contact->getTokenRowArrayByUser($user_id);

        if (!empty($access_token_row_array)) {
            $client->setAccessToken(json_decode($access_token_row_array['token_json'], 1));
            if ($client->isAccessTokenExpired()) {

                $expired = true;
                $refresh_token = "";

                if (!in_array($access_token_row_array['refresh_token'], $this->false_empty_or_null_arr)) {
                    $refresh_token = $access_token_row_array['refresh_token'];
                }

                $client->fetchAccessTokenWithRefreshToken($refresh_token);

                $accessToken = $client->getAccessToken();

                $accessToken_data = array();

                $accessToken_data['access_token'] = $accessToken['access_token'];
                $accessToken_data['token_type'] = $accessToken['token_type'];
                $accessToken_data['expires_in'] = $accessToken['expires_in'];
                $accessToken_data['created'] = $accessToken['created'];

                $accessToken_data['user_id'] = $this->ion_auth->get_user_id();
                $accessToken_data['expires_at'] = $accessToken['created'] + $accessToken['expires_in'];
                $accessToken_data['token_json'] = json_encode($accessToken);

                $this->db->where('user_id', $user_id);
                $this->db->update('user_google_client_token', $accessToken_data);

            }
            //----------------------------------------------------------------------------------------------------------

            $service = new Google_Service_PeopleService($client);

            $optParams = array();
            $optParams['pageToken'] = '';
            $optParams['pageSize'] = 2000;
            $optParams['personFields'] = 'names,emailAddresses,urls';

            $results = $service->people_connections->listPeopleConnections('people/me', $optParams);


            if (count($results->getConnections()) == 0) {
                $this->session->set_flashdata('error', 'error');
                $this->session->set_flashdata('no_contact_found', 'No contact is found');
                redirect('admin/contact');

            }

            $batch_contact_data = array();
            /*echo "<pre>";
            print_r($results);
            echo "</pre>";*/
            foreach ($results->getConnections() as $person) {

                $display_name = '';
                $family_name = '';
                $given_name = '';
                $email = '';
                $gender = '';
                $birthday = '';
                $address = '';

                //---------------------------------------------

                $names = $person->getNames();
                if (!empty($names)) {
                    $name = end($names);
                    if ($name) {
                        $display_name = $name->getDisplayName();
                        $family_name = $name->getFamilyName();
                        $given_name = $name->getGivenName();
                    }
                }


                $email_addresses = $person->getEmailAddresses();
                if (!empty($email_addresses)) {
                    $email_address = end($email_addresses);

                    if ($email_address) {
                        $email = $email_address->getValue();
                    }
                }

                $genders = $person->getGenders();
                if (!empty($genders)) {
                    $a_gender = end($genders);

                    if ($a_gender) {
                        $gender = $a_gender->getDisplayName();
                    }
                }

                $birthdays = $person->getBirthdays();
                if (!empty($birthdays)) {
                    $a_birthday = end($birthdays);

                    if ($a_birthday) {
                        $birthday = $a_birthday->getDisplayName();
                    }
                }

                $addresses = $person->getAddresses();
                if (!empty($addresses)) {
                    $an_addresses = end($addresses);

                    if ($an_addresses) {
                        $address = $an_addresses->getDisplayName();
                    }
                }

                //---------------------------------------------------
                $single_contact_data = array();
                if ($family_name != "") {
                    $single_contact_data['firstName'] = $given_name;
                    $single_contact_data['lastName'] = $family_name;
                    $single_contact_data['email'] = $email;
                    $single_contact_data['contact_holder_id'] = $this->ion_auth->get_user_id();
                }


                $contact_exist = false;
                if (!empty($single_contact_data)) {
                    $contact_exist = $this->contact->checkIfUserContactExist($single_contact_data);
                }




                if (!$contact_exist && !empty($single_contact_data)) {
                    $batch_contact_data[] = $single_contact_data;
                    $this->db->insert('contact', $single_contact_data);

                }

                //echo "<pre>zz";
                //print_r($email_addresses);
                //echo "zz</pre>";
                //echo "<br><hr><br>";
                //echo "<< $display_name | $family_name | $given_name | email[ $email ] | $gender | $birthday | $address >> <br>";
                //echo "<pre>";
                //print_r($person);
                //echo "</pre>";
                //echo "<br><hr><br>";
            }
            //exit;

            if (!empty($batch_contact_data)) {
                //$this->db->insert_batch('contact', $batch_contact_data); //not now
            } else {
                $this->session->set_flashdata('success', 'success');
                $this->session->set_flashdata('no_new_contact_found', 'No new contact has been found');
                redirect('admin/contact');
            }

        }

        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('contact_insert_success', 'Contacts inserted successfully');
        redirect('admin/contact');


        //----------------------------------------------------------------------------------------------------------


    }


}