<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Owner extends Admin_Controller {
//https://mbahcoding.com/tutorial/php/codeigniter/codeigniter-ajax-crud-using-bootstrap-modals-and-datatable.html
   public function __construct()
    {
        parent::__construct();
        $this->load->model('owner_model','owner');

        $this->load->helper('url');
        /* Load :: Common */
        //$this->lang->load('admin/users');

        /* Title Page :: Common */
        $this->page_title->push(lang('menu_owner'));
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('menu_owner'), 'admin/users');
    }

    public function index()
    {
        if (! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /* Load Template */
            $this->template->admin_render('admin/owner/owner_view', $this->data);
        }
    }

    public function ajax_list()
    {
        $list = $this->owner->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $owner) {
            $no++;
            $row = array();
            $row[] = $owner->user_fname;
            $row[] = $owner->user_lname;
            $row[] = $owner->phone;
            $row[] = $owner->email;
            $row[] = $owner->user_status?"Active":"Inactive";
            $row[] = $owner->created_at;

            //add html for action
            $owner->action = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_owner('."'".$owner->user_id."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_owner('."'".$owner->user_id."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
            $row[] = $owner->action;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->owner->count_all(),
            "recordsFiltered" => $this->owner->count_filtered(),
            "data" => $data,
        );
        //output to json format

        echo json_encode($output);
        die();
    }

    public function ajax_edit($id)
    {
        $data = $this->owner->get_by_id($id);
        //$data->password = $this->decryptIt($data->password);
        echo json_encode($data);
        die();
    }

    public function ajax_add()
    {
        $this->_validate($change_password = true);
        $this->_validate($change_password);
        $data = array(
            'user_fname' => $this->input->post('user_fname'),
            'user_lname' => $this->input->post('user_lname'),
            'phone' => $this->input->post('phone'),
            'email' => $this->input->post('email'), //must check email not duplicate
            'user_status' => $this->input->post('user_status'),
        );

        $data['role_type'] = 1; //adding owner

        if($change_password){
            $data['password'] = $this->encryptIt(trim($this->input->post('password')));
        }

        $insert = $this->owner->save($data);
        echo json_encode(array("status" => TRUE));
        die();
    }

    public function ajax_update()
    {
        $change_password = false;

        if(trim($this->input->post('password')) != ''){
            $change_password = true;
        }

        $this->_validate($change_password);
        $data = array(
            'user_fname' => $this->input->post('user_fname'),
            'user_lname' => $this->input->post('user_lname'),
            'phone' => $this->input->post('phone'),
            'user_status' => $this->input->post('user_status'),
        );

        if($change_password){
            $data['password'] = $this->encryptIt(trim($this->input->post('password')));
        }

        $this->owner->update(array('user_id' => $this->input->post('user_id')), $data);
        echo json_encode(array("status" => TRUE));
        die();
    }

    public function ajax_delete($id)
    {
        $this->owner->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
        die();
    }


    private function _validate($change_password)
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if($this->input->post('user_fname') == '')
        {
            $data['inputerror'][] = 'user_fname';
            $data['error_string'][] = 'First name is required';
            $data['status'] = FALSE;
        }

        if($this->input->post('user_lname') == '')
        {
            $data['inputerror'][] = 'user_lname';
            $data['error_string'][] = 'Last name is required';
            $data['status'] = FALSE;
        }

        if($this->input->post('user_status') == '')
        {
            $data['inputerror'][] = 'user_status';
            $data['error_string'][] = 'Please select user status';
            $data['status'] = FALSE;
        }

        if($change_password){

            if($this->input->post('password') == '')
            {
                $data['inputerror'][] = 'password';
                $data['error_string'][] = 'Password name is required';
                $data['status'] = FALSE;
            }

            if($this->input->post('confirm_password') == '')
            {
                $data['inputerror'][] = 'confirm_password';
                $data['error_string'][] = 'Confirm password name is required';
                $data['status'] = FALSE;
            }

            if($this->input->post('password') != $this->input->post('confirm_password'))
            {
                $data['inputerror'][] = 'Password';
                $data['error_string'][] = 'Password and Confirm Password does not match';
                $data['status'] = FALSE;
            }
        }



        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }

    //match the crypt key with main bloom rent site's key
    function decryptIt($q) {
        $cryptKey = 'Lf6Q5htqdgnSn0AABqlsSddj1QNu0fJs';
        $qDecoded = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), base64_decode($q), MCRYPT_MODE_CBC, md5(md5($cryptKey))), "\0");
        return( $qDecoded );
    }

    function encryptIt($q) {
        $cryptKey = 'Lf6Q5htqdgnSn0AABqlsSddj1QNu0fJs';
        $qEncoded = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), $q, MCRYPT_MODE_CBC, md5(md5($cryptKey))));
        return( $qEncoded );
    }

}