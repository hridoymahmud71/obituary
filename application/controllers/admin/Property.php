<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Property extends Admin_Controller {
//https://mbahcoding.com/tutorial/php/codeigniter/codeigniter-ajax-crud-using-bootstrap-modals-and-datatable.html
   public function __construct()
    {
        parent::__construct();
        $this->load->model('property_model','property');

        $this->load->helper('url');
        /* Load :: Common */
        //$this->lang->load('admin/users');

        /* Title Page :: Common */
        $this->page_title->push(lang('menu_property'));
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('menu_property'), 'admin/users');
    }

    public function index()
    {
        if (! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            $this->data['states'] = $this->property->getStatesByCountry($country_id = 13);//australian states

            /* Load Template */
            $this->template->admin_render('admin/property/property_view', $this->data);
        }
    }

    public function ajax_list()
    {
        $list = $this->property->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $property) {
            $no++;
            $row = array();

            $property->owner_name = $property->user_fname.' '.$property->user_lname;

            $row[] = $property->owner_name;
            $row[] = $property->email;
            $row[] = $property->phone;
            $row[] = $property->property_address;
            $row[] = $property->city;
            $row[] = $property->zip_code;
            $row[] = $property->state_name;
            $row[] = $property->created_at;

            //add html for action
            $property->action = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_property('."'".$property->property_id."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_property('."'".$property->property_id."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
            $row[] = $property->action;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->property->count_all(),
            "recordsFiltered" => $this->property->count_filtered(),
            "data" => $data,
        );
        //output to json format

        echo json_encode($output);
        die();
    }

    public function ajax_edit($id)
    {
        $data = $this->property->get_by_id($id);
        //$data->dob = ($data->dob == '0000-00-00') ? '' : $data->dob; // if 0000-00-00 set tu empty for datepicker compatibility
        echo json_encode($data);
        die();
    }

    public function ajax_add()
    {
        //$this->_validate();

        $data = array(
            'property_address' => $this->input->post('property_address'),
            'city' => $this->input->post('city'),
            'zip_code' => $this->input->post('zip_code'),
            'state' => $this->input->post('state'),
        );
        $insert = $this->property->save($data);
        echo json_encode(array("status" => TRUE));
        die();
    }

    public function ajax_update()
    {
        $this->_validate();
        $data = array(
            'property_address' => trim($this->input->post('property_address')),
            'city' => trim($this->input->post('city')),
            'zip_code' => trim($this->input->post('zip_code')),
            'state' => $this->input->post('state'),
        );
        $this->property->update(array('property_id' => $this->input->post('property_id')), $data);
        echo json_encode(array("status" => TRUE));
        die();
    }

    public function ajax_delete($id)
    {
        $this->property->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
        die();
    }


    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        /*
         'property_address' => $this->input->post('property_address'),
            'city' => $this->input->post('city'),
            'zip_code' => $this->input->post('zip_code'),
            'state' => $this->input->post('state'),
        */

        if(trim($this->input->post('property_address')) == '')
        {
            $data['inputerror'][] = 'property_address';
            $data['error_string'][] = 'Property Address is required';
            $data['status'] = FALSE;
        }

        if($this->input->post('city') == '')
        {
            $data['inputerror'][] = 'city';
            $data['error_string'][] = 'City is required';
            $data['status'] = FALSE;
        }

        if($this->input->post('state') == '')
        {
            $data['inputerror'][] = 'state';
            $data['error_string'][] = 'State is required';
            $data['status'] = FALSE;
        }

        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }

}