<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends Admin_Controller
{
    //https://mbahcoding.com/tutorial/php/codeigniter/codeigniter-ajax-crud-using-bootstrap-modals-and-datatable.html
    public function __construct()
    {
        parent::__construct();
        $this->load->model('email_model', 'emod');

        $this->load->helper('url');
        /* Load :: Common */
        //$this->lang->load('admin/users');

        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }

        /* Title Page :: Common */
        $this->page_title->push(lang('menu_email'));
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('menu_email'), 'admin/users');
    }

    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /* Load Template */
            $this->template->admin_render('admin/email/email_view', $this->data);
        }
    }

    public function ajax_list()
    {

        $contact_holder_id = $this->ion_auth->get_user_id();

        $list = $this->emod->get_datatables($contact_holder_id);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $email) {
            $no++;
            $row = array();
            $row[] = $email->firstName;
            $row[] = $email->lastName;
            $row[] = $email->email;

            //add html for action
            $email->action = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_email(' . "'" . $email->id . "'" . ')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_email(' . "'" . $email->id . "'" . ')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
            $row[] = $email->action;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->emod->count_all_for_a_contact_holder($contact_holder_id),
            "recordsFiltered" => $this->emod->count_filtered_for_a_contact_holder($contact_holder_id),
            "data" => $data,
        );
        //output to json format

        echo json_encode($output);
        die();
    }

    public function ajax_edit($id)
    {
        $data = $this->emod->get_by_id($id);
        echo json_encode($data);
        die();
    }

    public function ajax_add()
    {
        $this->_validate();
        $data = array(
            'firstName' => $this->input->post('firstName'),
            'lastName' => $this->input->post('lastName'),
            'email' => $this->input->post('email'),
            'created_on' => strtotime('now'),
            'contact_holder_id' => $this->ion_auth->get_user_id()
        );
        $insert = $this->emod->save($data);
        echo json_encode(array("status" => TRUE));
        die();
    }

    public function ajax_update()
    {
        $this->_validate();
        $data = array(
            'firstName' => $this->input->post('firstName'),
            'lastName' => $this->input->post('lastName'),
            'email' => $this->input->post('email'),
            'updated_on' => strtotime('now')
        );
        $this->emod->update(array('id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
        die();
    }

    public function ajax_delete($id)
    {
        $this->emod->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
        die();
    }


    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('firstName') == '') {
            $data['inputerror'][] = 'firstName';
            $data['error_string'][] = 'First name is required';
            $data['status'] = FALSE;
        }

        if ($this->input->post('lastName') == '') {
            $data['inputerror'][] = 'lastName';
            $data['error_string'][] = 'Last name is required';
            $data['status'] = FALSE;
        }

        if ($this->input->post('email') == '') {
            $data['inputerror'][] = 'email';
            $data['error_string'][] = 'Email is required';
            $data['status'] = FALSE;
        }

        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }

}