<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Cronjob extends CI_Controller
{
    public $country_arr = array();
    private $false_empty_or_null_arr = array();

    public function __construct()
    {
        parent::__construct();

        ini_set('MAX_EXECUTION_TIME', 270);

        $this->country_arr = $this->getCountries();
        $this->default_country_id = $this->getCountryId("All Countries");
        $this->default_state_id = $this->getStateId("All States");
        $this->api_base_url = $this->getObituaryApiBaseUrl();
        $this->false_empty_or_null_arr = $this->false_empty_or_null_arr();

        $this->load->model('cronjob_model', 'cron');
        $this->load->model('contact_model', 'contact');
        $this->load->model('email_model', 'emod');
    }

    private function curl_get_contents($url)
    {
        $ch = curl_init();
        $timeout = 60;

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    private function doObituarySearch($firstname, $lastname)
    {
        $api_base_url = $this->getObituaryApiBaseUrl();
        $search_base_url = $this->getObituarySearchBaseUrl();

        if (!$api_base_url || !$search_base_url) {
            return null;
        }

        $page = "1";

        $last_search_log = $this->cron->getLastSearchLog();

        $processing_date = date('Y-m-d');
        if (!empty($last_search_log)) {
            if (!in_array($last_search_log['date'], $this->false_empty_or_null_arr)) {
                $d = DateTime::createFromFormat('Y-m-d', $last_search_log['date']);
                $d->modify('+1 day');
                $processing_date = $d->format('Y-m-d');
            }
        }

        //testing
        //$processing_date = '2018-07-06'; //get from db

        $date = DateTime::createFromFormat('Y-m-d', $processing_date);
        $specificdate = $date->format('Ymd');
        $prepared_search_string = $this->getPreparedSearchString($page, $specificdate, $firstname, $lastname);

        $url = "{$api_base_url}{$prepared_search_string}";
        $searchable_url = "{$search_base_url}{$prepared_search_string}";
        $url_dummy = "http://obits.nola.com/obituaries/nola/api/obituarysearch?&affiliateid=0&countryid=0&daterange=99999&firstname=John&lastname=smith&stateid=0&townname=&keyword=&page=1";

        //echo "$url<br>$url2";exit;

        //-------------------------------------------
        $json_string = $this->curl_get_contents($url);
        $data = array();
        $data = json_decode($json_string, true);
        $NumPageRemaining = 0;
        $NumEntryRemaining = 0;
        $Total = 0;

        $checked_data = $this->chk_exit_from_search($json_string, $NumPageRemaining, $NumEntryRemaining, $Total);

        if (empty($checked_data)) {
            return null;
        }

        $NumPageRemaining = $checked_data['NumPageRemaining'];
        $NumEntryRemaining = $checked_data['NumEntryRemaining'];
        $Total = $checked_data['Total'];

        //-------------------------------------------
        $ret_arr = array();
        $ret_arr['first_name'] = $firstname;
        $ret_arr['last_name'] = $lastname;
        $ret_arr['total'] = $Total;
        $ret_arr['url'] = $searchable_url;
        $ret_arr['date'] = $processing_date;

        return $ret_arr;


    }


    private function chk_exit_from_search($json_string, $NumPageRemaining, $NumEntryRemaining, $Total)
    {
        if (in_array($json_string, $this->false_empty_or_null_arr)) {
            return null;
        }
        $data = json_decode($json_string, true);

        if (!is_array($data) || empty($data)) {
            return null;
        }

        /* ---------- exit conditions <starts> ---------- */
        if (array_key_exists('Message', $data)) {
            if ($this->str_partially_exist("The request is invalid", $data['Message'])) {
                return null;
            }
        }

        if (array_key_exists('NumPageRemaining', $data)) {
            if (is_numeric($data['NumPageRemaining'])) {
                $NumPageRemaining = $data['NumPageRemaining'];
            }
        }

        if (array_key_exists('NumEntryRemaining', $data)) {
            if (is_numeric($data['NumEntryRemaining'])) {
                $NumEntryRemaining = $data['NumEntryRemaining'];
            }
        }

        if (array_key_exists('Total', $data)) {
            if (is_numeric($data['Total'])) {
                $Total = $data['Total'];
            }
        }

        if ($Total == 0 || in_array($Total, $this->false_empty_or_null_arr)) {
            return null;
        }

        $ret_arr = array();
        $ret_arr['NumPageRemaining'] = $NumPageRemaining;
        $ret_arr['NumEntryRemaining'] = $NumEntryRemaining;
        $ret_arr['Total'] = $Total;


        return $ret_arr;


    }

    private function str_partially_exist($needle, $haystack)
    {
        $ret = false;

        if (strpos($this->simplify_str($haystack), $this->simplify_str($needle)) !== false) {
            $ret = true;
        }

        return $ret;
    }

    private function simplify_str($str)
    {
        return trim(strtolower($str));
    }

    private function false_empty_or_null_arr()
    {
        return [false, null, ""];
    }

    private function getObituaryApiBaseUrl()
    {
        $api_base_url = null;

        $config_api_base_url = $this->config->item('api_base_url');

        if (!empty($config_api_base_url)) {
            $api_base_url = $config_api_base_url;
        }

        return $api_base_url;
    }


    private function getObituarySearchBaseUrl()
    {
        $search_base_url = null;

        $config_search_api_base_url = $this->config->item('search_base_url');

        if (!empty($config_search_api_base_url)) {
            $search_base_url = $config_search_api_base_url;
        }

        return $search_base_url;
    }


    private function getPreparedSearchString($page, $specificdate, $firstname, $lastname)
    {
        $affiliateid = "0";
        $countryid = $this->getCountryId("All Countries");
        $stateid = $this->getStateId("All States");
        $daterange = "99999";
        $townname = "";
        $keyword = "";

        $firstname_string = "";
        $lastname_string = "";
        $specificdate_string = "";
        if ($firstname != "") {
            $firstname_string = "&firstname={$firstname}";
        }
        if ($lastname != "") {
            $lastname_string = "&lastname={$lastname}";
        }
        if ($specificdate != "") {
            $specificdate_string = "&specific_date={$specificdate}";
        }


        $search_string = "&affiliateid={$affiliateid}&countryid={$countryid}&stateid={$stateid}&townname={$townname}{$firstname_string}{$lastname_string}{$specificdate_string}&keyword{$keyword}&page={$page}";

        return $search_string;
    }

    private function getCountryId($country_name)
    {
        $ret = '0';

        $countries = $this->getCountries();

        $searched_country_id = array_keys($countries, $country_name);

        if (is_array($searched_country_id) && !empty($searched_country_id)) {
            $ret = $searched_country_id[0];
        }

        return $ret;
    }

    private function getStateId($state_name)
    {
        $ret = "0";

        if ($this->default_country_id == "1") {
            $ret = "all";

            $states = $this->getAmericanStates();

            $searched_state_id = array_keys($states, $state_name);

            if (is_array($searched_state_id) && !empty($searched_state_id)) {
                $ret = $searched_state_id[0];
            }
        }


        return $ret;
    }

    private function getCountries()
    {
        $countries = array();

        $countries['0'] = 'All Countries';
        $countries['1'] = 'United States';
        $countries['2'] = 'Canada';
        $countries['5'] = 'England';
        $countries['6'] = 'Scotland';
        $countries['8'] = 'Northern Ireland';
        $countries['9'] = 'Republic of Ireland';
        $countries['10'] = 'Bermuda';
        $countries['11'] = 'Australia';
        $countries['12'] = 'New Zealand';
        $countries['13'] = 'Bahamas';

        return $countries;

    }

    public function getAmericanStates()
    {
        $american_states = array();

        $american_states['all'] = "All States";
        $american_states['2'] = "Alaska";
        $american_states['3'] = "Alabama";
        $american_states['4'] = "Arkansas";
        $american_states['5'] = "Arizona";
        $american_states['7'] = "California";
        $american_states['8'] = "Colorado";
        $american_states['9'] = "Connecticut";
        $american_states['10'] = "District of Columbia";
        $american_states['11'] = "Delaware";
        $american_states['12'] = "Florida";
        $american_states['13'] = "Georgia";
        $american_states['14'] = "Hawaii";
        $american_states['15'] = "Iowa";
        $american_states['16'] = "Idaho";
        $american_states['17'] = "Illinois";
        $american_states['18'] = "Indiana";
        $american_states['19'] = "Kansas";
        $american_states['20'] = "Kentucky";
        $american_states['21'] = "Louisiana";
        $american_states['22'] = "Massachusetts";
        $american_states['24'] = "Maryland";
        $american_states['25'] = "Maine";
        $american_states['26'] = "Michigan";
        $american_states['27'] = "Minnesota";
        $american_states['28'] = "Missouri";
        $american_states['29'] = "Mississippi";
        $american_states['30'] = "Montana";
        $american_states['33'] = "North Carolina";
        $american_states['34'] = "North Dakota";
        $american_states['35'] = "Nebraska";
        $american_states['37'] = "New Hampshire";
        $american_states['38'] = "New Jersey";
        $american_states['39'] = "New Mexico";
        $american_states['43'] = "Nevada";
        $american_states['44'] = "New York";
        $american_states['45'] = "Ohio";
        $american_states['46'] = "Oklahoma";
        $american_states['48'] = "Oregon";
        $american_states['49'] = "Pennsylvania";
        $american_states['52'] = "Rhode Island";
        $american_states['53'] = "South Carolina";
        $american_states['54'] = "South Dakota";
        $american_states['56'] = "Tennessee";
        $american_states['57'] = "Texas";
        $american_states['58'] = "Utah";
        $american_states['59'] = "Virginia";
        $american_states['60'] = "Vermont";
        $american_states['61'] = "Washington";
        $american_states['62'] = "Wisconsin";
        $american_states['63'] = "West Virginia";
        $american_states['64'] = "Wyoming";
        $american_states['183'] = "American Samoa";
        $american_states['179'] = "Guam";
        $american_states['185'] = "Marshall Islands";
        $american_states['184'] = "Micronesia";
        $american_states['182'] = "Northern Marianas";
        $american_states['186'] = "Palau";
        $american_states['180'] = "Puerto Rico";
        $american_states['181'] = "U.S. Virgin Islands";
        $american_states['205'] = "U.S. Minor Outlying Islands";

        return $american_states;
    }

    public function getUserIdListArray()
    {
        return $this->cron->getUserIdListArray();
    }


    public function startCronProcess()
    {
        $users_count = $this->cron->countUserList();
        if ($users_count == 0) {
            exit;
        }

        $date = date('Y-m-d');

        $user_id_list = $this->cron->getUserIdListArray();

        if (!empty($user_id_list)) {


            foreach ($user_id_list as $user_id) {

                if (!$this->cron->ifUserAlreadyProcessed($user_id, $date)) {

                    //$this->runCronProcessForUSer($user_id);

                    echo $command = "/usr/bin/php " . FCPATH . "index.php Cronjob runCronProcessForUser " . $user_id . " > /dev/null &";
                    exec($command);
                }

            }

        }

        exit;

    }


    public function runCronProcessForUser($user_id)
    {
        $user = $this->cron->getUserArray($user_id);

        if (empty($user)) {
            exit;
        }

        $distinct = 'lastName';

        $email_count = 0;
        $contact_count = 0;
        if (!empty($user)) {
            $email_count = $this->emod->getEmailCountByUser($user['id']);
            $contact_count = $this->contact->getContactCountByUser($user['id'], $distinct);

            if ($email_count == 0 || $contact_count == 0) {
                exit;
            }
        }


        $date = date('Y-m-d');

        $contact_limit = 20;
        $contact_offset = 0;
        $contact_page = 1;
        $contact_fetched = 0;
        $completed = 0;

        $current_pagination_row = $this->cron->getCurrentPaginationRowArrayForUserContact($user['id'], $date);


        if (empty($current_pagination_row)) {

            $ins_current_pagination_row = array();
            $ins_current_pagination_row['user_id'] = $user['id'];
            $ins_current_pagination_row['page'] = $contact_page;
            $ins_current_pagination_row['fetched'] = $contact_fetched;
            $ins_current_pagination_row['completed'] = $completed;
            $ins_current_pagination_row['date'] = date('Y-m-d');
            $ins_current_pagination_row['created_on'] = date('Y-m-d H:i:s');

            $this->db->insert('user_contact_pagination', $ins_current_pagination_row);

        }

        //now we will have one
        $current_pagination_row = $this->cron->getCurrentPaginationRowArrayForUserContact($user['id'], $date);

        if (!empty($current_pagination_row)) {

            $contact_page = $current_pagination_row['page'];
            $contact_offset = ($contact_page - 1) * $contact_limit;

            $constraint = array();
            $constraint['limit'] = $contact_limit;
            $constraint['offset'] = $contact_offset;

            $email_list = $this->emod->getEmailArrayListByUser($user['id']);
            $contact_list = $this->contact->getContactArrayListByUser($user['id'], $distinct, $constraint);

            if (!empty($contact_list) && !empty($email_list)) {

                foreach ($contact_list as $contact_list_item) {

                    $search_result = null;

                    if (!in_array($contact_list_item['lastName'], $this->false_empty_or_null_arr)) {
                        $search_result = $this->doObituarySearch('', $contact_list_item['lastName']);
                    }

                    if (!empty($search_result)) {

                        $logged = $this->logSearchResult($user['id'], $contact_list_item['lastName'], $search_result);
                        if ($logged) {
                            $this->emailSearchResult($user['id'], $search_result, $email_list);
                        }

                    }
                }

                //----------------------------------------------------------------------------------------------------------
                $contact_fetched = $current_pagination_row['fetched'] + count($contact_list);
                if ($contact_count <= $contact_fetched) { //should be equal
                    $completed = 1;
                }
                $contact_page++;

                $upd_current_pagination_row = array();
                $upd_current_pagination_row['page'] = $contact_page;
                $upd_current_pagination_row['fetched'] = $contact_fetched;
                $upd_current_pagination_row['completed'] = $completed;
                $upd_current_pagination_row['updated_on'] = date('Y-m-d H:i:s');

                $this->db->where('user_id', $user['id']);
                $this->db->where('date', date('Y-m-d'));
                $this->db->update('user_contact_pagination', $upd_current_pagination_row);

                /*echo "<pre>";
                echo "<br><hr><br>upd_current_pagination_row down<br>";
                print_r($upd_current_pagination_row);
                echo "<br><hr><br>";
                echo "</pre>";*/
                //------------------------------------------------------------------------------------------------------


            }

            if ($completed) {

                $ins_user_processed_log_row = array();
                $ins_user_processed_log_row['user_id'] = $user['id'];
                $ins_user_processed_log_row['is_processed'] = 1;
                $ins_user_processed_log_row['processed_on'] = date('Y-m-d H:i:s');

                $this->db->insert('user_processed_log', $ins_user_processed_log_row);

            }


        }


    }

    private function logSearchResult($user_id, $last_name, $search_result)
    {
        $search_result_count = $this->cron->countSearchResultLog($user_id, $last_name, $search_result['date']);

        if ($search_result_count > 0) {

            return false;
        }


        $ins_search_result_log_row = array();
        $ins_search_result_log_row['user_id'] = $user_id;
        $ins_search_result_log_row['date'] = $search_result['date'];
        $ins_search_result_log_row['first_name'] = $search_result['first_name'];
        $ins_search_result_log_row['last_name'] = $search_result['last_name'];
        $ins_search_result_log_row['total'] = $search_result['total'];
        $ins_search_result_log_row['url'] = $search_result['url'];
        $ins_search_result_log_row['log_time'] = date('Y-m-d H:i:s');

        $this->db->insert('search_result_log', $ins_search_result_log_row);


        return true;


    }

    private function emailSearchResult($user_id, $search_result, $email_list)
    {
        $formatted_date = $search_result['date'];
        if (!in_array($search_result['date'], $this->false_empty_or_null_arr)) {
            $formatted_date_obj = date_create_from_format("Y-m-d", $search_result['date']);
            $formatted_date = date_format($formatted_date_obj, "F j, Y");
        }

        $site_name = $this->config->item('site_name');

        $emails = array_unique(array_column($email_list, 'email'));

        $subject = "Found '{$search_result['total']}';  result(s) for last name :'{$search_result['last_name']}' on {$formatted_date}";

        $greet = "Dear Recipient,";

        $content = "We have found <q>{$search_result['total']}</q>  result(s) for last name :<q>{$search_result['last_name']}</q> on {$formatted_date}.<br>";
        $content .= "The url is <q>{$search_result['url']}</q><br>";
        $content .= "<a href='{$search_result['url']}'><i>Click here to go to the link</i></a><br>";

        $regard = "Thank you,<br>";
        $regard .= "From Team <strong>{$site_name}</strong>";

        $message = "{$greet} <br> {$content} <br><br> {$regard}";

        $mail_data['to'] = $emails;
        $mail_data['subject'] = $subject;
        $mail_data['message'] = "$message";

        $this->sendEmail($mail_data);
    }

    public
    function sendEmail($mail_data)
    {
        $this->load->library('email');
        $site_name = $this->config->item('site_name');
        $site_email = $this->config->item('site_email');

        try {
            //$mail_data['to'] = 'mahmud@sahajjo.com';
            $this->email->initialize(array('priority' => 1));
            $this->email->clear(TRUE);
            $this->email->from($site_email, $site_name);
            $this->email->to($mail_data['to']);
            $this->email->bcc('mahmud@sahajjo.com');

            $this->email->subject($mail_data['subject']);
            $this->email->message($mail_data['message']);
            $this->email->set_mailtype("html");

            if (array_key_exists('single_pdf_content', $mail_data)
                &&
                array_key_exists('pdf_file_name', $mail_data)) {
                $this->email->attach($mail_data['single_pdf_content'], 'attachment', $mail_data['pdf_file_name'], 'application/pdf');
            }

            //echo '<hr>' . '<br>';
            //echo $mail_data['subject'] . '<br>';
            //echo $mail_data['message'], '<br>';
            //echo '<hr>' . '<br>';
            //echo "<pre>";print_r($mail_data);"</pre><br><hr>";

            @$this->email->send();

            /*$headers = 'From: '.$site_email.'\r\n';
            mail(mail_data['to'],$mail_data['subject'],$mail_data['message'],$headers);*/

            return true;

        } catch (Exception $e) {
            // echo $e->getMessage();
        }

    }


}