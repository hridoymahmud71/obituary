<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Contact_model extends CI_Model
{
    private $false_empty_or_null_arr = array();

    var $table = 'contact';
    var $column_order = array('firstname', 'lastname', 'email', 'gender', 'address', 'dob', null); //set column field database for datatable orderable
    var $column_search = array('firstname', 'lastname', 'email', 'address'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('id' => 'desc'); // default order

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->false_empty_or_null_arr = $this->false_empty_or_null_arr();
    }

    private function false_empty_or_null_arr()
    {
        return [false, null, ""];
    }

    private function _get_datatables_query($contact_holder_id = null)
    {

        $this->db->from($this->table);
        if ($contact_holder_id) {
            $this->db->where('contact_holder_id', $contact_holder_id);
        }

        $i = 0;

        foreach ($this->column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables($contact_holder_id = null)
    {
        $this->_get_datatables_query($contact_holder_id);
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function count_all_for_a_contact_holder($contact_holder_id)
    {
        $this->db->from($this->table);
        $this->db->where('contact_holder_id', $contact_holder_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_filtered_for_a_contact_holder($contact_holder_id)
    {
        $this->_get_datatables_query($contact_holder_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function get_by_id($id)
    {
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $query = $this->db->get();

        return $query->row();
    }

    public function save($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function delete_by_id($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    //------------------------------------------------------------------------------------------------------------------

    private function getTokenQueryByUser($user_id)
    {
        $this->db->select('*');
        $this->db->from('user_google_client_token');
        $this->db->where('user_id', $user_id);
        $this->db->order_by('user_google_client_token_id', 'desc');

        $query = $this->db->get();
        return $query;
    }

    public function getTokenRowByUser($user_id)
    {
        $query = $this->getTokenQueryByUser($user_id);
        return $query->row();
    }

    public function getTokenRowArrayByUser($user_id)
    {
        $query = $this->getTokenQueryByUser($user_id);
        return $query->row_array();
    }

    public function getAccessTokenByUser($user_id)
    {
        $access_token = '';
        $token = $this->getTokenRowArrayByUser($user_id);

        if (!empty($token)) {
            if (array_key_exists('access_token', $token)) {
                if ($token['access_token'] != '') {
                    $access_token = $token['access_token'];
                }
            }
        }

        return $access_token;
    }

    //------------------------------------------------------------------------------------------------------------------
    private function getContactListQueryByUser($user_id, $distinct, $constraint)
    {
        if (!in_array($distinct, $this->false_empty_or_null_arr)) {
            $this->db->select(" distinct(lastName)");
        } else {
            $this->db->select('lastName');

        }
        $this->db->from($this->table);
        $this->db->where('contact_holder_id', $user_id);

        if (!empty($constraint) && is_array($constraint)) {

            if (array_key_exists('limit', $constraint) && array_key_exists('offset', $constraint)) {

                if (is_numeric($constraint['limit']) && is_numeric($constraint['offset'])) {
                    $this->db->limit($constraint['limit'], $constraint['offset']);
                }

            } else if (array_key_exists('limit', $constraint) && !array_key_exists('offset', $constraint)) {

                if (is_numeric($constraint['limit'])) {
                    $this->db->limit($constraint['limit']);
                }

            }

        }

        $query = $this->db->get();
        return $query;
    }

    public function getContactObjectListByUser($user_id, $distinct, $constraint)
    {
        $query = $this->getContactListQueryByUser($user_id, $distinct, $constraint);
        return $query->result();
    }

    public function getContactArrayListByUser($user_id, $distinct, $constraint)
    {
        $query = $this->getContactListQueryByUser($user_id, $distinct, $constraint);
        return $query->result_array();
    }

    public function getContactCountByUser($user_id, $distinct)
    {
        $query = $this->getContactListQueryByUser($user_id, $distinct, null);
        return $query->num_rows();
    }

    //------------------------------------------------------------------------------------------------------------------

    public function checkIfUserContactExist($single_contact_data)
    {
        if(empty($single_contact_data)){
            return false;
        }

        if (in_array($single_contact_data['contact_holder_id'], $this->false_empty_or_null_arr)) {
            return false;
        }

        //-------------------------------------------------

        $chk_thr_email = false;

        $fn = "";
        $ln = "";

        if (!in_array($single_contact_data['firstName'], $this->false_empty_or_null_arr)) {
            $fn = $single_contact_data['firstName'];
        }
        if (!in_array($single_contact_data['lastName'], $this->false_empty_or_null_arr)) {
            $ln = $single_contact_data['lastName'];
        }

        $concated_names = trim($fn) . " " . trim($ln);
        //--------------------------------------------------

        $this->db->select('*');
        $this->db->from($this->table);



        $this->db->where('contact_holder_id', $single_contact_data['contact_holder_id']);

        if (!empty($single_contact_data) && is_array($single_contact_data)) {


            if (!in_array($single_contact_data['email'], $this->false_empty_or_null_arr)) {
                $this->db->where('email', $single_contact_data['email']);
                $chk_thr_email = true;
            }

            if (!$chk_thr_email) {

                if (($fn == "" && $ln == "") || $concated_names == "") {
                    $this->db->flush_cache(); //so next query do not face a problem
                    return false;
                }

                if ($fn != "" || $ln != "") {
                    $this->db->where("CONCAT(firstName,' ',lastName) =", $concated_names);
                }


            }


        }

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        if ($num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

}