<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email_model extends CI_Model
{

    var $table = 'email';
    var $column_order = array('firstname', 'lastname', 'email', null); //set column field database for datatable orderable
    var $column_search = array('firstname', 'lastname', 'email'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('id' => 'desc'); // default order

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query($contact_holder_id = null)
    {
        if ($contact_holder_id) {
            $this->db->where('contact_holder_id', $contact_holder_id);
        }

        $this->db->from($this->table);

        $i = 0;

        foreach ($this->column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables($contact_holder_id = null)
    {
        $this->_get_datatables_query($contact_holder_id);
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function count_all_for_a_contact_holder($contact_holder_id)
    {
        $this->db->from($this->table);
        $this->db->where('contact_holder_id', $contact_holder_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_filtered_for_a_contact_holder($contact_holder_id)
    {
        $this->_get_datatables_query($contact_holder_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function get_by_id($id)
    {
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $query = $this->db->get();

        return $query->row();
    }

    public function save($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function delete_by_id($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    //------------------------------------------------------------------------------------------------------------------

    private function getEmailListQueryByUser($user_id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('contact_holder_id', $user_id);

        $query = $this->db->get();
        return $query;
    }

    public function getEmailObjectListByUser($user_id)
    {
        $query = $this->getEmailListQueryByUser($user_id);
        return $query->result();
    }

    public function getEmailArrayListByUser($user_id)
    {
        $query = $this->getEmailListQueryByUser($user_id);
        return $query->result_array();
    }

    public function getEmailCountByUser($user_id)
    {
        $query = $this->getEmailListQueryByUser($user_id);
        return $query->num_rows();
    }


}