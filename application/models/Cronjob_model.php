<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cronjob_model extends CI_Model
{
    public $false_empty_or_null_arr = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

        $this->false_empty_or_null_arr = $this->false_empty_or_null_arr();
    }

    public function false_empty_or_null_arr()
    {
        return [false, null, ""];
    }

    public function getUserListQuery()
    {
        $this->db->select('*');
        $this->db->from('users');

        $query = $this->db->get();
        return $query;
    }

    public function getUserObjectList()
    {
        $query = $this->getUserListQuery();
        return $query->result();
    }

    public function getUserArrayList()
    {
        $query = $this->getUserListQuery();
        return $query->result_array();
    }

    public function countUserList()
    {
        $query = $this->getUserListQuery();
        return $query->num_rows();
    }

    //------------
    public function getUserQuery($user_id)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id', $user_id);

        $query = $this->db->get();
        return $query;
    }

    public function getUserObject($user_id)
    {
        $query = $this->getUserQuery($user_id);
        return $query->row();
    }

    public function getUserArray($user_id)
    {
        $query = $this->getUserQuery($user_id);
        return $query->row_array();
    }

    //----------
    public function getUserIdListArray()
    {
        $id_list = array();

        $this->db->select('id');
        $this->db->from('users');

        $query = $this->db->get();
        $result_array = $query->result_array();

        if (!empty($result_array)) {
            $id_list = array_column($result_array, 'id');
        }

        return $id_list;

    }


    //------------------------------------------------------------------------------------------------------------------

    public function getLastProcessedUserQueryByDate($date)
    {
        $this->db->select('*');
        $this->db->from('user_processed_log');
        $this->db->where("DATE(user_processed_log.processed_on)", $date);
        $this->db->order_by("user_processed_log.processed_on", 'desc');
        $query = $this->db->get();
        return $query;
    }

    public function getLastProcessedUserRowByDate($date)
    {
        $query = $this->getLastProcessedUserQueryByDate($date);
        return $query->row();
    }

    public function getLastProcessedUserRowArrayByDate($date)
    {
        $query = $this->getLastProcessedUserQueryByDate($date);
        return $query->row_array();
    }

    public function ifUserAlreadyProcessed($user_id, $date)
    {
        $ret = false;

        $this->db->select('*');
        $this->db->from('user_processed_log');
        $this->db->where("user_id", $user_id);
        $this->db->where("DATE(user_processed_log.processed_on)", $date);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        if ($num_rows > 0) {
            $ret = true;
        }

        return $ret;
    }

    //------------------------------------------------------------------------------------------------------------------

    public function getNextUserQueryToProcess($last_processed_user_id)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where("id >", $last_processed_user_id);
        $this->db->order_by("users.id", 'asc');
        $query = $this->db->get();
        return $query;
    }

    public function getNextUserRowToProcess($last_processed_user_id)
    {
        $query = $this->getNextUserQueryToProcess($last_processed_user_id);
        return $query->row();
    }

    public function getNextUserRowArrayToProcess($last_processed_user_id)
    {
        $query = $this->getNextUserQueryToProcess($last_processed_user_id);
        return $query->row_array();
    }

    //------------------------------------------------------------------------------------------------------------------

    public function getCurrentPaginationQueryForUserContact($user_id, $date = null)
    {
        $this->db->select('*');
        $this->db->from('user_contact_pagination');
        $this->db->where("user_id", $user_id);

        if ($date) {
            $this->db->where("date", $date);
        }


        $query = $this->db->get();
        return $query;
    }

    public function getCurrentPaginationRowForUserContact($user_id, $date = null)
    {
        $query = $this->getCurrentPaginationQueryForUserContact($user_id, $date);
        return $query->row();
    }

    public function getCurrentPaginationRowArrayForUserContact($user_id, $date = null)
    {
        $query = $this->getCurrentPaginationQueryForUserContact($user_id, $date);
        return $query->row_array();
    }

    //------------------------------------------------------------------------------------------------------------------

    public function getSearchResultLogQuery($user_id, $last_name, $date = null)
    {
        $this->db->select('*');
        $this->db->from('search_result_log');
        $this->db->where("user_id", $user_id);

        if (!in_array($last_name, $this->false_empty_or_null_arr)) {
            $this->db->where("last_name", $last_name);
        }

        if ($date) {
            $this->db->where("date", $date);
        }

        $query = $this->db->get();
        return $query;
    }

    public function getSearchResultLogRow($user_id, $last_name, $date = null)
    {
        $query = $this->getSearchResultLogQuery($user_id, $last_name, $date);
        return $query->row();
    }

    public function getSearchResultLogRowArray($user_id, $last_name, $date = null)
    {
        $query = $this->getSearchResultLogQuery($user_id, $last_name, $date);
        return $query->row_array();
    }

    public function countSearchResultLog($user_id, $last_name, $date = null)
    {
        $query = $this->getSearchResultLogQuery($user_id, $last_name, $date);
        return $query->num_rows();
    }

    public function getLastSearchLog()
    {
        $this->db->select('*');
        $this->db->from('search_result_log');

        $this->db->order_by('log_time', 'desc');

        $query = $this->db->get();
        return $query->row_array();
    }
    
    //------------------------------------------------------------------------------------------------------------------


}